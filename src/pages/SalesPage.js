import React, {Component} from 'react';
import Form from '../components/Form';
import Cart from '../components/Cart';

import './styles/SalesPage.css';


class SalesPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            errorsubmit: '',
            form: {
                firstname: '',
                firstnameerror: '',
                lastname: '',
                lastnameerror: '',
                email: '',
                emailerror: '',
                phone: '',
                phoneerror: '',
                codpostal: '',
                codpostalerror: '',
                colony: '',
                colonyerror: '',
                states: '',
                stateserror: '',
                city: '',
                cityerror: '',
                depm: '',
                depmerror: '',
                street: '',
                streeterror: '',
                colonies: [],
                isMany: false,
                isError: false,

            }
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);     
        this.number = parseFloat('10599');
        this.number = this.number.toFixed(2);   
    }
    

    valRegExp = (targetName) => {
        switch(targetName) {
            case 'email':
                return {
                    regularExpression: RegExp(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/),
                    errorMessage: 'Introduzca una dirección de correo eletrónico válida',
                };
            case 'firstname':
                return { 
                    //regularExpression: RegExp(/\b([A-ZÀ-ÿ][-,a-z. ']+[ ]*)+/gm),
                    regularExpression: RegExp(/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u),
                    errorMessage: 'Introduzca un nombre válido',
                };
            case 'lastname':
                return {
                    regularExpression: RegExp(/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u),
                    errorMessage: 'Introduzca apellidos válidos',
                };
            case 'codpostal':
                return {
                    regularExpression: RegExp(/^[0-9]{5}(?:-[0-9]{4})?$/),
                    errorMessage: 'Introduzca codigo postal válidos',
                };
            case 'phone':
                return {
                    //regularExpression: RegExp(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im),
                    regularExpression: RegExp(/^[1-9]\d{6}$/),
                    errorMessage: 'Introduzca un número telefónico válido',
                };
            case 'colony':
                return {
                    regularExpression: RegExp(/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/),
                    errorMessage: 'Introduzca una colonia valida',
                }; 
            case 'states':
                return {
                    regularExpression: RegExp(/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/),
                    errorMessage: 'Introduzca un estado valido',
                }; 
            case 'city':
                return {
                    regularExpression: RegExp(/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/),
                    errorMessage: 'Introduzca un ciudad valido',
                };  
            case 'depm':
                return {
                    regularExpression: RegExp(/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/),
                    errorMessage: 'Introduzca un departamento valido',
                }; 
            case 'street':
                return {
                    regularExpression: '',
                    errorMessage: '',
                };                              
            default:
                return {
                    regularExpression: '',
                    errorMessage: '',
                };
        }
    }

    handleValidationError = (e, message) => {
        const attrMessage = `${e.target.name}error`;

        this.setState({
            form:{
                ...this.state.form,
                [e.target.name]: e.target.value,
                [attrMessage]: message,
            }
        });
    }

    handleValidationErrorWithoutarget = (e, message) => {
        const attrMessage = e;

        this.setState({
            form:{
                ...this.state.form,
                [attrMessage]: message,
                isError: true,
            }
        });
    }    


    handleChange = async e => {      

        let rege = this.valRegExp(e.target.name);

        if(!e.target.value) {   

            this.handleValidationError(e, 'Este campo es obligatorio'); 
            
        }else if(rege.regularExpression === ''){
            this.setState({
                form: {
                    ...this.state.form,
                    [e.target.name]: e.target.value,
                    [`${e.target.name}error`]: '',
                }
            });
        }
        else if(!rege.regularExpression.test(this.removeAccents(e.target.value))) {
            this.handleValidationError(e, rege.errorMessage);

        }else {
            if(e.target.name === 'codpostal') 
            {
                let url = `https://blackisp.herokuapp.com/postalCodes/${e.target.value}`;                

                this.fetchData(url)
                .then((data)=>{
                    if(data){
                        if(data.colonies.length > 1) {
                            this.setState({
                                form: {
                                    ...this.state.form,
                                    states: data.state,
                                    city: data.city,
                                    depm: data.town,
                                    colony: data.colonies[0],
                                    colonies: data.colonies,
                                    isMany: true,
                                }
                            });
                        }else{
                            this.setState({
                                form: {
                                    ...this.state.form,
                                    states: data.state,
                                    city: data.city,
                                    depm: data.town,
                                    colony: data.colonies,
                                    colonies: data.colonies,
                                    isMany: false,
                                }
                            });
                        }
                        
                    }                                                   
                }).catch((error)=>{
                    console.log(error);                                       
                });

                this.setState({
                    form: {
                        ...this.state.form,
                        [e.target.name]: e.target.value,
                        [`${e.target.name}error`]: '',
                    }
                });

            }else
            {
                this.setState({
                    form: {
                        ...this.state.form,
                        [e.target.name]: e.target.value,
                        [`${e.target.name}error`]: '',
                    }
                });
            }            
        }        
    } 
    
    fetchData = async (url) => {
        try {
            const response = await fetch(url);
            const json = await response.json();
            return json;
        } catch (error) {
            console.log('[cart error] ', error);
        }
    };
    
    removeAccents = (str) => {
        return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    }

    handleSubmit = async (e) => {
        e.preventDefault();
        let iserror = false;
        const regemail = this.valRegExp('email');
        const regfirstname = this.valRegExp('firstname');
        const reglastname = this.valRegExp('lastname');
        const regcodpostal = this.valRegExp('codpostal');
        const regphone = this.valRegExp('phone');
        const regcolony = this.valRegExp('colony');
        const regstates = this.valRegExp('states');
        const regcity = this.valRegExp('city');
        const regdepm = this.valRegExp('depm');

        if(this.state.form.email === '') {
            this.handleValidationErrorWithoutarget('emailerror', 'Este campo es obligatorio');
            iserror = true;
        }

        if(this.state.form.firstname === '') {
            this.handleValidationErrorWithoutarget('firstnameerror', 'Este campo es obligatorio');
            iserror = true;
        }   

        if(this.state.form.lastname === '') {
            this.handleValidationErrorWithoutarget('lastnameerror', 'Este campo es obligatorio');
            iserror = true;
        }   

        if(this.state.form.codpostal === '') {
            this.handleValidationErrorWithoutarget('codpostalerror', 'Este campo es obligatorio');
            iserror = true;
        }

        if(!regemail.regularExpression.test(this.state.form.email))
        {
            this.handleValidationErrorWithoutarget('emailerror', regemail.errorMessage);
            iserror = true;
        }

        if(!regfirstname.regularExpression.test(this.state.form.firstname))
        {
            this.handleValidationErrorWithoutarget('firstnameerror', regfirstname.errorMessage);
            iserror = true;
        }  

        if(!reglastname.regularExpression.test(this.state.form.lastname))
        {
            this.handleValidationErrorWithoutarget('lastnameerror', reglastname.errorMessage);
            iserror = true;
        } 

        if(!regcodpostal.regularExpression.test(this.state.form.codpostal))
        {
            this.handleValidationErrorWithoutarget('codpostalerror', regcodpostal.errorMessage);
            iserror = true;
        }

        if(!regphone.regularExpression.test(this.state.form.phone))
        {
            this.handleValidationErrorWithoutarget('phoneerror', regphone.errorMessage);
            iserror = true;
        }  

        if(!regcolony.regularExpression.test(this.removeAccents(this.state.form.colony)))
        {
            this.handleValidationErrorWithoutarget('colonyerror', regcolony.errorMessage);
            iserror = true;
        } 

        if(!regcity.regularExpression.test(this.removeAccents(this.state.form.city)))
        {
            this.handleValidationErrorWithoutarget('cityerror', regcity.errorMessage);
            iserror = true;
        } 

        if(!regdepm.regularExpression.test(this.state.form.depm))
        {
            this.handleValidationErrorWithoutarget('depmerror', regdepm.errorMessage);
            iserror = true;
        }                                                         

        if(!iserror) {
            let body = {
                firstname: this.state.form.firstname,
                lastname: this.state.form.lastname,
                email: this.state.form.email,
                phone: this.state.form.phone,
                codpostal: this.state.form.codpostal,
                colony: this.state.form.colony,
                states: this.state.form.states,
                city: this.state.form.city,
                depm: this.state.form.depm,
                street: this.state.form.street,
            }

            try {
                let req = await fetch ('https://blackisp.herokuapp.com/contact', {
                    method: "POST",
                    body
                });
                let json = await req.json();
            } catch (error) {
                this.setState({
                    errorsubmit: "upss.. hay un problema porfavor intentarlo mas tarde",
                });

                console.log(error);

            }
        }    
    }

    componentDidMount() {

    }

    render() {       

        return(
            <div id="container" className="display-flex justify-center">
                <div className="contenedor-box">                    
                    <Form 
                        formValue={this.state.form}
                        errorSubmit={this.state.errorsubmit}
                        onHandleChange={this.handleChange}
                        onHandleSubmit={this.handleSubmit}
                    />
                                       
                    <Cart />
                </div>
            </div>
        );
    }

}

export default SalesPage;