import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import SalesPage from '../pages/SalesPage';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={SalesPage} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
