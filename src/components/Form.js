import React, {Component} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { 
    faUser, 
    faEnvelope, 
    faPhoneAlt, 
    faMapMarkerAlt, 
    faMapMarkedAlt 
} from '@fortawesome/free-solid-svg-icons';

import './styles/Form.css';

class Form extends Component {
    constructor(props){
        super(props);
    }

    componentDidMount() {

    }


    render() {

        const { 
            firstnameerror, 
            lastnameerror, 
            emailerror, 
            phoneerror,
            codpostalerror,
            colonyerror,
            stateserror,
            cityerror,
            depmerror,
            streeterror
        } = this.props.formValue;      
          
        return(            
            <div id="address-direction" className="item-1-box">
                <div className="address-container-title">
                    <h2 className="address-title">DIRECCIÓN DE ENVÍO</h2>
                </div>
                <div className="container-inputs">
                    {this.props.errorSubmit !== '' && (
                        <p className="error">{this.props.errorSubmit}</p>
                    )}      

                    <form onSubmit={this.props.onHandleSubmit}>
                        <section className="contenedor">
                                    

                            <div className="item-1">
                                <div className="display-flex padding-r-4">
                                    <div className="input-group-text justify-center">
                                        <FontAwesomeIcon icon={faUser} />
                                    </div>

                                    <input 
                                        className={"input-main"}
                                        type="text" 
                                        placeholder="Nombre" 
                                        onChange={this.props.onHandleChange} 
                                        name="firstname" 
                                        value={this.props.formValue.firstname} 
                                    />
                                </div>

                                {firstnameerror !== '' &&(
                                                <small className="error">{firstnameerror} </small>                                                        
                                            )
                                } 
                            </div>

                            <div className="item-2">
                                <div className="display-flex padding-l-4">
                                    <div className="input-group-text justify-center">
                                        <FontAwesomeIcon icon={faUser} />
                                    </div>

                                    <input 
                                        className="input-main" 
                                        type="text" 
                                        placeholder="Apellidos" 
                                        onChange={this.props.onHandleChange} 
                                        name="lastname" 
                                        value={this.props.formValue.lastname} 
                                    />
                                </div>

                                {lastnameerror !== '' &&(
                                            <small className="error">{lastnameerror} </small> 
                                                
                                        )
                                }  
                            </div>  

                            <div className="item-3">
                                <div className="display-flex padding-r-4">
                                    <div className="input-group-text justify-center">
                                        <FontAwesomeIcon icon={faEnvelope} />
                                    </div>

                                    <input 
                                        className="input-main" 
                                        type="email" 
                                        placeholder="Correo eletronico" 
                                        onChange={this.props.onHandleChange} 
                                        name="email" 
                                        value={this.props.formValue.email} 
                                    />
                                </div>

                                {emailerror !== '' &&(
                                                <small className="error">{emailerror} </small>                                                        
                                            )
                                } 
                            </div>  

                            <div className="item-4">
                                <div className="display-flex padding-l-4">
                                    <div className="input-group-text justify-center">
                                        <FontAwesomeIcon icon={faPhoneAlt} />
                                    </div>

                                    <input 
                                        className="input-main" 
                                        type="text" 
                                        placeholder="Número de teléfono" 
                                        onChange={this.props.onHandleChange} 
                                        name="phone" 
                                        value={this.props.formValue.phone} 
                                    />
                                </div>  
                                {phoneerror !== '' &&(
                                                <small className="error">{phoneerror} </small>                                                        
                                            )
                                }                                                                       
                            </div> 

                            <div className="item-5">
                                <div className="display-flex padding-r-4">
                                    <div className="input-group-text justify-center">
                                        <FontAwesomeIcon icon={faMapMarkerAlt} />
                                    </div>
                                    <input 
                                        className="input-main" 
                                        type="text" 
                                        placeholder="Código Postal" 
                                        onChange={this.props.onHandleChange} 
                                        name="codpostal" 
                                        value={this.props.formValue.codpostal}
                                    />                                                                            
                                </div> 

                                {codpostalerror !== '' &&(
                                                <small className="error">{codpostalerror} </small>                                                        
                                            )
                                }                                     
                            </div> 

                            <div className="item-6">
                                <div className="display-flex padding-l-4">
                                        <div className="input-group-text justify-center">
                                            <FontAwesomeIcon icon={faMapMarkerAlt} />
                                        </div>
                                        {this.props.formValue.isMany ? (
                                            <select name="colony" className="width-100" value={this.props.formValue.colony} onChange={this.props.onHandleChange}>
                                                {this.props.formValue.colonies.map((selects)=>                                                   
                                                    <option value={selects} key={selects}>{selects}</option>
                                                )}
                                            </select>
                                        ) : (
                                            <input 
                                                className="input-main" 
                                                type="text" 
                                                placeholder="Colonia" 
                                                onChange={this.props.onHandleChange} 
                                                name="colony" 
                                                value={this.props.formValue.colony} 
                                            /> 
                                        )}
                                                                                                                   
                                    </div> 

                                    {colonyerror !== '' &&(
                                                    <small className="error">{colonyerror} </small>                                                        
                                                )
                                    }                                 
                            </div>  

                            <div className="item-7">
                                <div className="display-flex padding-r-4">
                                        <div className="input-group-text justify-center">
                                            <FontAwesomeIcon icon={faMapMarkerAlt} />
                                        </div>
                                        <input 
                                            className="input-main" 
                                            type="text" 
                                            placeholder="Estado/Region" 
                                            onChange={this.props.onHandleChange} 
                                            name="states" 
                                            value={this.props.formValue.states} 
                                        />                                                                            
                                    </div> 

                                    {stateserror !== '' &&(
                                                    <small className="error">{stateserror} </small>                                                        
                                                )
                                    }                                 
                            </div> 

                            <div className="item-8">
                                <div className="display-flex padding-l-4">
                                        <div className="input-group-text justify-center">
                                            <FontAwesomeIcon icon={faMapMarkerAlt} />
                                        </div>
                                        <input 
                                            className="input-main" 
                                            type="text" 
                                            placeholder="Ciudad" 
                                            onChange={this.props.onHandleChange} 
                                            name="city" 
                                            value={this.props.formValue.city} 
                                        />                                                                            
                                    </div> 

                                    {cityerror !== '' &&(
                                                    <small className="error">{cityerror} </small>                                                        
                                                )
                                    }                                 
                            </div>     

                            <div className="item-9">
                                <div className="display-flex padding-r-4">
                                        <div className="input-group-text justify-center">
                                            <FontAwesomeIcon icon={faMapMarkerAlt} />
                                        </div>
                                        <input 
                                            className="input-main" 
                                            type="text" 
                                            placeholder="Delegación o municipio" 
                                            onChange={this.props.onHandleChange} 
                                            name="depm" 
                                            value={this.props.formValue.depm} 
                                        />                                                                            
                                    </div> 

                                    {depmerror !== '' &&(
                                                    <small className="error">{depmerror} </small>                                                        
                                                )
                                    }                                 
                            </div>   

                            <div className="item-10">
                                <div className="display-flex padding-l-4">
                                        <div className="input-group-text justify-center">
                                            <FontAwesomeIcon icon={faMapMarkedAlt} />
                                        </div>
                                        <input 
                                            className="input-main" 
                                            type="text" 
                                            placeholder="Calle" 
                                            onChange={this.props.onHandleChange} 
                                            name="street" 
                                            value={this.props.formValue.street} 
                                        />                                                                            
                                    </div> 

                                    {streeterror !== '' &&(
                                                    <small className="error">{streeterror} </small>                                                        
                                                )
                                    }                                 
                            </div>
                        </section>

                        <div className="display-flex margin-b-5 margin-t-15">                                                      
                            <button className="button-black">Libreta de direcciones</button>
                            <button className="button-black">Guardar</button>
                        </div>

                        <input type="checkbox" id="facturacion" />  
                        <label className="check-input" htmlFor="facturacion">
                            Utilizar dirección de facturación
                        </label>
                    </form>
                </div>                        
            </div>
        );
    }
}

export default Form;