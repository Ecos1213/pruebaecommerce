import React from 'react';
import NumberFormat from "react-number-format";

import './styles/Products.css';

const Products = (props) => {
    return(
        <section className="contenedor-cart">
            <div className={`item-1-cart`}>
                <img src={props.productsValue.image} alt="" className="full-width" />
            </div>
            <div className={`item-2-cart`}>
                <p className="name-product">{props.productsValue.name}</p>
            </div>
            <div className={`item-3-cart`}>
                <p className="price-product">
                    <b>
                        <NumberFormat
                            thousandsGroupStyle="thousand"
                            value={parseFloat(props.productsValue.price).toFixed(2)}
                            prefix="$"
                            decimalSeparator="."
                            displayType="text"
                            type="text"
                            thousandSeparator={true}
                            allowNegative={true}
                            isNumericString={true} 
                            renderText={(value, props) => 
                                <span {...props}>                                                
                                        <span>{value.split('.')[0]+'.'}</span>
                                        <span className="small-dot-decimal">{value.split('.')[1]}</span>
                                    
                                </span>
                            }
                        />    
                    </b>                            
                </p>
            </div>                            
        </section>
    );
}

export default Products;