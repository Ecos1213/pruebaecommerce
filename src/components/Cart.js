import React, {useEffect, useState} from 'react';
import Products from './Products';
import NumberFormat from "react-number-format";
import './styles/Cart.css';

const Cart = (props) => {
    const [data, setData] = useState([]);
    let total = 0;
    let increment = 1;

    useEffect(()=>{
        const url = 'https://blackisp.herokuapp.com/products';

        const fetchData = async () => {
            try {
                const response = await fetch(url);
                const json = await response.json();
                //console.log(json);
                setData(json);
            } catch (error) {
                console.log('[cart error] ', error);
            }
        };

        fetchData();

    },[]);

    data.map((products)=> total = total + parseFloat(products.price));
    

    return(

        <div id="cart" className="item-2-box">                        
            <div className="cart-container-title">
                <h2 className="cart-title">RESUMEN DE LA ORDEN</h2>
            </div> 

            <div>
                {data.map((products) =>                       
                    <Products 
                        key={increment += increment}
                        productsValue={products}                    
                    />
                )}

                <section className="contenedor-cart">
                    <div className="item-1-cart">
                    </div>
                    <div className="item-2-cart">
                    </div>
                    <div className="item-3-cart">
                        <button type="button" className="botton-red">Editar</button>
                    </div>                            
                </section>

                <section className="contenedor-cart background-gray padding-right-left">
                    <div className="item-1-cart">
                        <p className="margin-0 roboto-regular-family"><b>SUBTOTAL</b></p>
                        <p className="margin-0 roboto-regular-family"><b>ENVIO</b></p>
                    </div>
                    <div className="item-2-cart">
                    </div>
                    <div className="item-3-cart text-end">
                        <p className="margin-0 roboto-regular-family">
                            <b>
                                <NumberFormat
                                    thousandsGroupStyle="thousand"
                                    value={parseFloat(total).toFixed(2)}
                                    prefix="$"
                                    decimalSeparator="."
                                    displayType="text"
                                    type="text"
                                    thousandSeparator={true}
                                    allowNegative={true}
                                    isNumericString={true} 
                                    renderText={(value, props) => 

                                        <span {...props}>
                                            <span>{value.split('.')[0]+'.'}</span>
                                            <span className="small-dot-decimal">{value.split('.')[1]}</span>
                                        </span>

                                    }
                                />
                            </b>
                        </p>
                        <p className="margin-0 roboto-regular-family"><b>A calcular</b></p>
                    </div>                            
                </section>

                <section className="contenedor-cart background-black padding-right-left">
                    <div className="item-1-cart">
                        <p className="margin-0 roboto-regular-family"><b>TOTAL</b></p>
                    </div>
                    <div className="item-2-cart">
                    </div>
                    <div className="item-3-cart text-end">
                        <p className="margin-0 roboto-regular-family">
                            <b>
                                <NumberFormat
                                    thousandsGroupStyle="thousand"
                                    value={parseFloat(total).toFixed(2)}
                                    prefix="$"
                                    decimalSeparator="."
                                    displayType="text"
                                    type="text"
                                    thousandSeparator={true}
                                    allowNegative={true}
                                    isNumericString={true} 
                                    renderText={(value, props) => 

                                        <span {...props}>
                                            <span>{value.split('.')[0]+'.'}</span>
                                            <span className="small-dot-decimal">{value.split('.')[1]}</span>
                                        </span>

                                    }
                                />
                            </b>
                        </p>
                    </div>                            
                </section>            
            </div>        
        </div>        
        
    );
}

export default Cart;